package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * Class describing a train departure.
 * Contains a constructor to create a train departure and all associated attributes.
 * The class includes getters for the attributes, and setters for attributes that are
 * logical to mutate.
 * Goal: act as a model of a train departure.
 *
 * @author Anders Janin Ellingsen
 */
public class TrainDeparture {

  private final LocalTime departureTime;
  private final String line;
  private final int trainNumber;
  private final String destination;
  private int track;
  private int delayHours;
  private int delayMinutes;

  /**
   * Constructs a train departure by assigning the parameters below as attributes of the train departure.
   *
   * @param departureTimeHours is an integer value for which hour of the day for the train departure.
   * @param departureTimeMinutes is an integer value for which minute of the day for the train departure.
   * @param line is a combination of letters and integers in a String for describing the line.
   * @param trainNumber is a unique integer for each train.
   * @param destination is a String representing the train departure's destination.
   * @param track is an integer representing the track for the train departure.
   * @param delayHours is an integer representing the amount of whole hours a train departure is delayed.
   * @param delayMinutes is an integer representing the amount of remaining minutes a train departure is delayed.
   *
   * @throws IllegalArgumentException if any of the parameters are not within its valid range.
   */

  public TrainDeparture(int departureTimeHours, int departureTimeMinutes, String line,
                          int trainNumber, String destination, int track, int delayHours, int delayMinutes) {
    validateInput(departureTimeHours, departureTimeMinutes, trainNumber, track, delayHours, delayMinutes);

    this.departureTime = LocalTime.of(departureTimeHours, departureTimeMinutes);
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = track;
    this.delayHours = delayHours;
    this.delayMinutes = delayMinutes;
  }

  /**
   * Verifies a given Integer for every parameter. departureTimeHours has to be between 0 and 23.
   * departureTimeMinutes has to be between 0 and 59. trainNumber has to be positive. track has to
   * be positive or -1. delayHours and delayMinutes has to be non-negative. The check fails if
   * any of the parameters are not within its valid range.
   *
   * @param departureTimeHours is an integer value for which hour of the day for the train departure.
   * @param departureTimeMinutes is an integer value for which minute if the day for the train departure.
   * @param trainNumber is a unique integer for each train.
   * @param track is an integer representing the track for the train departure.
   * @param delayHours is an integer representing the amount of whole hours a train departure is delayed.
   * @param delayMinutes is an integer representing the amount of remaining minutes a train departure is delayed.
   *
   * @throws IllegalArgumentException if any of the parameters are not within its valid range.
   */
  private void validateInput(int departureTimeHours, int departureTimeMinutes, int trainNumber,
                               int track, int delayHours, int delayMinutes) {
    if (departureTimeHours < 0 || departureTimeHours > 23 || departureTimeMinutes < 0 || departureTimeMinutes > 59) {
      throw new IllegalArgumentException("Invalid departure time.");
    }

    if (trainNumber <= 0) {
      throw new IllegalArgumentException("Invalid train number. Has to be positive.");
    }

    if (track <= 0 && track != -1) {
      throw new IllegalArgumentException("Invalid track number. Has to be positive or -1.");
    }

    if (delayHours < 0 || delayMinutes < 0) {
      throw new IllegalArgumentException("Delay hours and minutes must be non-negative.");
    }
  }

  /**
   * Returns the LocalTime representing time of the train departure.
   *
   * @return LocalTime in hh/mm.
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Returns the String representing the train departure's line.
   *
   * @return a String with a combination of letters and integers.
   */
  public String getLine() {
    return line;
  }

  /**
   * Returns an integer representing the train number.
   *
   * @return an integer of the train number.
   */
  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   * Returns a string representing the name of the destination for the train departure.
   *
   * @return a String with the name of the destination.
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Returns an integer representing the track for the train departure.
   *
   * @return an integer for the track.
   */
  public int getTrack() {
    return track;
  }

  /**
   * Returns a new LocalTime with added delay for the train departure.
   *
   * @return LocalTime for a delayed train departure.
   */
  public LocalTime getDelayedDepartureTime() {
    return departureTime.plusHours(delayHours).plusMinutes(delayMinutes);
  }

  /*
  Setters are only used to change information on a train departure that requires frequent updating
  such as track, delayHours and delayMinutes.
  */

  /**
   * Sets an integer value as the track for this existing object of the TrainDeparture class.
   * This allows for editing the track for the TrainDeparture.
   *
   * @param track is an integer value.
   *
   * @throws IllegalArgumentException if the attempted new track integer is inputted as
   *                                  a negative value except -1.
   */
  public void setTrack(int track) {
    if (track <= 0 && track != -1) {
      throw new IllegalArgumentException("Invalid track number. Has to be positive or -1.");
    }
    this.track = track;
  }

  /**
   * Sets the delayed departure time for this existing object of the TrainDeparture class.
   * This allows for editing the delay of the TrainDeparture.
   *
   * @param delayHours is an integer representing the amount of whole hours a train departure is delayed.
   * @param delayMinutes is an integer representing the amount of remaining minutes a train departure is delayed.
   *
   * @throws IllegalArgumentException if either of the input parameters are negative.
   */
  public void setDelayedDepartureTime(int delayHours, int delayMinutes) {
    if (delayHours < 0 || delayMinutes < 0) {
      throw new IllegalArgumentException("Delay hours and minutes must be non-negative");
    }
    this.delayHours = delayHours;
    this.delayMinutes = delayMinutes;
  }

  /**
   * Returns a string representation of this Train departure.
   * The string includes information about the train's departure time, line, destination,
   * train number, and, if available, the track information. If the train is delayed,
   * the delayed departure time is also included in the string.
   *
   * @return A string representation of this Train departure.
   */
  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(departureTime).append("   ")
                .append(line).append("   ")
                .append("   Train number: ").append(trainNumber).append("   ")
                .append(destination);
    if (track != -1) {
      stringBuilder.append("   Track: ").append(track);
    }
    if (getDelayedDepartureTime().isAfter(departureTime)) {
      stringBuilder.append("   Delayed, new time: ").append(getDelayedDepartureTime());
    }
    return stringBuilder.toString();
  }
}