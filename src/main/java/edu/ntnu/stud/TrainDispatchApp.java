package edu.ntnu.stud;

/**
 * This is the main class for the train dispatch application.
 * Goal: launch the user interface via a main method.
 */
public class TrainDispatchApp {

  /**
   * Main-method that runs the application.
   *
   * @param args are the arguments for the main-method.
   */
  public static void main(String[] args) {

    UserInterface ui = new UserInterface();

    ui.init();
    ui.start();
  }
}