package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.*;


/**
 * Class describing a train register with information on all train departures.
 * Goal: act as a model for a train register.
 *
 * @author Anders Janin Ellingsen
 */
public class TrainRegister {

  private final HashMap<Integer, TrainDeparture> trainRegister = new HashMap<>();

  private LocalTime currentTime = LocalTime.of(0, 0);

  /**
   * Constructs an object of the class TrainRegister.
   */
  public TrainRegister() {
  }

  /**
   * Adds a new train departure to the current train register based on all the required parameters
   * to create a train departure. If a train departure with the inputted train number already
   * exists, then the method will not add the train departure and returns a String saying that.
   *
   * @param departureTimeHours is an integer value for which hour of the day for the train departure.
   * @param departureTimeMinutes is an integer value for which minute of the day for the train departure.
   * @param line is a combination of letters and integers in a String for describing the line.
   * @param trainNumber is a unique integer for each train.
   * @param destination is a String representing the train departure's destination.
   * @param track is an integer representing the track for the train departure.
   * @param delayHours is an integer representing the amount of whole hours a train departure is delayed.
   * @param delayMinutes is an integer representing the amount of remaining minutes a train departure is delayed.
   *
   * @return a String describing the outcome of the method.
   *
   * @throws IllegalArgumentException if any of the parameters are not within its valid range.
   */
  public String addTrainDeparture(int departureTimeHours, int departureTimeMinutes, String line,
                                  int trainNumber, String destination, int track,
                                  int delayHours, int delayMinutes) {

    for (Integer trainDeparture : trainRegister.keySet()) {
      if (trainDeparture == trainNumber) {
        return "Train number " + trainNumber + " already exists.";
      }
    }
    try {
      trainRegister.put(trainNumber, new TrainDeparture(departureTimeHours, departureTimeMinutes,
              line, trainNumber, destination, track, delayHours, delayMinutes));
    } catch (IllegalArgumentException e) {
      return "Error: " + e.getMessage();
    }
    return "Train departure has been added.";
  }

  /**
   * Removes a train departure from the current train register based on an inputted train number.
   * If a train departure with inputted train number does not exist, a String saying that will
   * be returned.
   *
   * @param trainNumber is a unique integer for each train.
   *
   * @return a String describing the outcome of the method.
   */
  public String removeTrainDeparture(int trainNumber) {
    for (Integer trainDeparture : trainRegister.keySet()) {
      if (trainDeparture == trainNumber) {
        trainRegister.remove(trainDeparture);
        return "Train departure with train number " + trainNumber + " has been removed.";
      }
    }
    return "There is no train departure with train number " + trainNumber + ".";
  }

  /**
   * Assigns a track to an already existing train departure in the current train register based
   * on inputted train number and track. If a train departure with inputted train number does
   * not exist, a String saying that will be returned.
   *
   * @param trainNumber is a unique integer for each train.
   * @param track is an integer representing the track for the train departure.
   *
   * @return a String describing the outcome of the method.
   *
   * @throws IllegalArgumentException if inputted track is negative and not -1.
   */
  public String assignTrack(int trainNumber, int track) {

    for (TrainDeparture trainDeparture : trainRegister.values()) {
      if (trainDeparture.getTrainNumber() == trainNumber) {
        try {
          trainDeparture.setTrack(track);
        } catch (IllegalArgumentException e) {
          return "Error: " + e.getMessage();
        }
        return "Train departure with train number " + trainDeparture.getTrainNumber()
                + " has been assigned to track " + track;
      }
    }
    return "Train departure with train number " + trainNumber + " not found.";
  }

  /**
   * Adds delay to a train departure's departure time based on inputted train number and delay.
   * If a train departure with inputted train number does not exist, a String saying that will
   * be returned.
   *
   * @param trainNumber is a unique integer for each train.
   * @param delayHours is an integer representing the amount of whole hours a train departure is delayed.
   * @param delayMinutes is an integer representing the amount of remaining minutes a train departure is delayed.
   *
   * @return a String describing the outcome of the method.
   *
   * @throws IllegalArgumentException if either delayHours or delayMinutes are negative.
   */
  public String enterDelay(int trainNumber, int delayHours, int delayMinutes) {
    for (TrainDeparture trainDeparture : trainRegister.values()) {
      if (trainDeparture.getTrainNumber() == trainNumber) {
        try {
          trainDeparture.setDelayedDepartureTime(delayHours, delayMinutes);
        } catch (IllegalArgumentException e) {
          return "Error: " + e.getMessage();
        }
        return "Train departure with train number " + trainDeparture.getTrainNumber()
                + " has been delayed by " + delayHours + " hours and " + delayMinutes + " minutes.";
      }
    }
    return "Train departure with train number " + trainNumber + " not found.";
  }

  /**
   * Finds and returns a train departure based on inputted train number. If a train departure with
   * inputted train number does not exist, a String saying that will be returned.
   *
   * @param trainNumber is a unique integer for each train.
   *
   * @return a String containing a train departure with the inputted train number.
   */
  public String findTrainDeparture(int trainNumber) {
    StringBuilder output = new StringBuilder();
    for (Integer trainDeparture : trainRegister.keySet()) {
      if (trainDeparture == trainNumber) {
        output.append(trainRegister.get(trainDeparture));
      }
    }
    if (output.isEmpty()) {
      return "There is no train departure with train number " + trainNumber + ".";
    } else {
      return output.toString();
    }

  }

  /**
   * Finds and returns all train departures with the inputted destination. If there are no
   * train departures with the inputted destination, a String saying that will be returned.
   *
   * @param destination is a String representing the train departure's destination.
   *
   * @return a String containing all train departures with the inputted destination.
   */
  public String findTrainDeparturesBasedOnDestination(String destination) {
    StringBuilder output = new StringBuilder();
    for (TrainDeparture trainDeparture : trainRegister.values()) {
      if (trainDeparture.getDestination().equalsIgnoreCase(destination)) {
        output.append(trainDeparture).append("\n");
      }
    }
    if (output.isEmpty()) {
      return "There is no train departure with " + destination + " as destination.";
    } else {
      return output.toString();
    }
  }

  /**
   * Updates the LocalTime for the train register. If the inputted time is earlier than the current
   * time, the LocalTime will not be changed, and a String saying that will be returned. All train
   * departures with departure time (included delay) earlier than the updated time will be removed
   * from the train register.
   *
   * @param departureTimeHours is an integer value for which hour of the day for the train departure.
   * @param departureTimeMinutes is an integer value for which minute of the day for the train departure.
   *
   * @return a String describing the outcome of the method.
   *
   * @throws IllegalArgumentException if departureTimeHours is not between 0 and 23, or if
   *                                  departureTimeMinutes is not between 0 and 59.
   */
  public String updateTime(int departureTimeHours, int departureTimeMinutes) {
    if (departureTimeHours < 0 || departureTimeHours > 23 || departureTimeMinutes < 0 || departureTimeMinutes > 59) {
      throw new IllegalArgumentException("Invalid departure time");
    }

    LocalTime updateTime;
    try {
      updateTime = LocalTime.of(departureTimeHours, departureTimeMinutes);
    } catch (IllegalArgumentException e) {
      return "Error: " + e.getMessage();
    }

    if (updateTime.isBefore(currentTime)) {
      return "It is not possible to set the time earlier than the current time (" + currentTime + ").";
    }

    currentTime = updateTime;

    // ChatGPT's suggestion to solving ConcurrentModificationException.
    List<Integer> keysToRemove = new ArrayList<>();

    for (Map.Entry<Integer, TrainDeparture> entry : trainRegister.entrySet()) {
      TrainDeparture trainDeparture = entry.getValue();

      if (trainDeparture.getDelayedDepartureTime().isBefore(currentTime)) {
        keysToRemove.add(entry.getKey());
      }
    }

    for (Integer key : keysToRemove) {
      trainRegister.remove(key);
    }

    return "The time has been updated to " + currentTime + ".";
  }

  /**
   * Sorts the current train register by departure time (excluded delay).
   *
   * @return a String containing all the current train departures sorted by departure time.
   */
  public String sortedTrainDeparturesByTime() {
    List<TrainDeparture> sortedTrainDepartures = trainRegister.values().stream()
            .sorted(Comparator.comparing(TrainDeparture::getDepartureTime))
            .toList();

    StringBuilder output = new StringBuilder("Departures" + "        Time: " + currentTime + "\n\n");
    for (TrainDeparture trainDeparture : sortedTrainDepartures) {
      output.append(trainDeparture).append("\n");
    }

    return output.toString();
  }

  /**
   * Creates objects of the TrainDeparture-class and puts them into the train register. Only used
   * for testing the application.
   */
  public void testData() {
    trainRegister.put(3, new TrainDeparture(10, 10, "F2", 3, "Oslo", 1, 0, 0));
    trainRegister.put(2, new TrainDeparture(12, 0, "L2", 2, "Trondheim", 2, 3, 0));
    trainRegister.put(1, new TrainDeparture(8, 30, "L1", 1, "Bergen", 3, 1, 20));
    trainRegister.put(4, new TrainDeparture(5, 30, "R2", 4, "Stavanger", -1, 0, 30));
  }
}