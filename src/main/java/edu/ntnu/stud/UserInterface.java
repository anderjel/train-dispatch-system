package edu.ntnu.stud;

import static javax.swing.JOptionPane.showInputDialog;
import static javax.swing.JOptionPane.showMessageDialog;

/**
 * UserInterface class to handle the interactions of the user.
 * Goal: interpret user input and act accordingly.
 *
 * @author Anders Janin Ellingsen
 */
public class UserInterface {
  TrainRegister trainRegister = new TrainRegister();

  /**
   * Constructs a new instance of the UserInterface class.
   * This constructor is used to create an object representing the user interface.
   */
  public UserInterface() {
  }

  /**
   * Initializes test data for the train register.
   */
  public void init() {
    trainRegister.testData();
  }

  /**
   * Initiates and manages the main loop for interacting with the train dispatch system.
   * The method continuously presents a menu to the user until the user chooses to exit
   * the application. The menu presents various operations and the user is prompted to
   * enter a digit between 1-9 to select an option from the menu. If an invalid choice
   * is entered, the user is informed to choose a valid option.
   */
  public void start() {

    while (true) {
      String choice = showInputDialog(
                """
                What do you want to do?
                1. Information board of the train departures
                2. Add train departure
                3. Remove train departure
                4. Assign a track to a train departure
                5. Enter a delay on a train departure
                6. Search for a train departure based on train number
                7. Search for a train departure based on destination
                8. Update the time
                9. Exit the application""");

      switch (choice) {
        case "1":
          showMessageDialog(null, trainRegister.sortedTrainDeparturesByTime());
          break;
        case "2":
          showMessageDialog(null, trainRegister.addTrainDeparture(
              Integer.parseInt(showInputDialog("Departure time hours (hh)")),
              Integer.parseInt(showInputDialog("Departure time minutes (mm)")),
              showInputDialog("Line"),
              Integer.parseInt(showInputDialog("Train number")), showInputDialog("Destination"),
              Integer.parseInt(showInputDialog("Track (Set to -1 if the train has not been allocated a track)")),
              Integer.parseInt(showInputDialog("Delay hours (hh)")),
              Integer.parseInt(showInputDialog("Delay minutes (mm)"))));
          break;
        case "3":
          showMessageDialog(null, trainRegister.removeTrainDeparture(
              Integer.parseInt(showInputDialog("Train number"))));
          break;
        case "4":
          showMessageDialog(null, trainRegister.assignTrack(
              Integer.parseInt(showInputDialog("Train number")),
              Integer.parseInt(showInputDialog("Assign track (Set to -1 if the train has not been allocated a track)"))));
          break;
        case "5":
          showMessageDialog(null, trainRegister.enterDelay(
              Integer.parseInt(showInputDialog("Train number")),
              Integer.parseInt(showInputDialog("Delay hours (hh)")),
              Integer.parseInt(showInputDialog("Delay minutes (mm)"))));
          break;
        case "6":
          showMessageDialog(null, trainRegister.findTrainDeparture(
              Integer.parseInt(showInputDialog("Train number"))));
          break;
        case "7":
          showMessageDialog(null, trainRegister.findTrainDeparturesBasedOnDestination(
                  showInputDialog("Destination")));
          break;
        case "8":
          showMessageDialog(null, trainRegister.updateTime(
              Integer.parseInt(showInputDialog("Time hours (hh)")),
              Integer.parseInt(showInputDialog("Time minutes (mm)"))));
          break;
        case "9":
          System.exit(0);
          break;
        default:
          showMessageDialog(null, "Please choose an option by entering a digit between 1-9");
      }
    }
  }
}