package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;

public class TrainDepartureTest {

  @Nested
  @DisplayName("Negative tests for 'TrainDeparture', throws exceptions on wrong input parameters")
  public class methodsThrowsExceptions {

    @Test
    @DisplayName("TrainDeparture-constructor throws 'Ill.Arg.Exc.' on invalid departure time")
    public void throwsOnInvalidDepartureTime() {
        try {
            // Arrange
            int departureTimeHours = -1;
            int departureTimeMinutes = 30;
            String line = "L1";
            int trainNumber = 1;
            String destination = "TestDestination";
            int track = 3;
            int delayHours = 1;
            int delayMinutes = 15;

            // Act
            new TrainDeparture(departureTimeHours, departureTimeMinutes, line,
                  trainNumber, destination, track, delayHours, delayMinutes);

            // Assert
            fail("The test 'throwsOnInvalidDepartureTime' failed since it did not throw"
                + " 'Ill.Arg.Exc.' as expected.");
        } catch (IllegalArgumentException e) {
            assertEquals("Invalid departure time.", e.getMessage());
        }
    }

    @Test
    @DisplayName("TrainDeparture-constructor throws 'Ill.Arg.Exc.' on invalid train number")
    public void throwsOnInvalidTrainNumber() {
        try {
            // Arrange
            int departureTimeHours = 12;
            int departureTimeMinutes = 30;
            String line = "L1";
            int trainNumber = -1;
            String destination = "TestDestination";
            int track = 3;
            int delayHours = 1;
            int delayMinutes = 15;

            // Act
            new TrainDeparture(departureTimeHours, departureTimeMinutes, line,
                    trainNumber, destination, track, delayHours, delayMinutes);

            // Assert
            fail("The test 'throwsOnInvalidTrainNumber' failed since it did not throw"
                + " 'Ill.Arg.Exc.' as expected.");
        } catch (IllegalArgumentException e) {
            assertEquals("Invalid train number. Has to be positive.", e.getMessage());
        }
    }

    @Test
    @DisplayName("TrainDeparture-constructor throws 'Ill.Arg.Exc.' on invalid track")
    public void throwsOnInvalidTrack() {
        try {
            // Arrange
            int departureTimeHours = 12;
            int departureTimeMinutes = 30;
            String line = "L1";
            int trainNumber = 1;
            String destination = "TestDestination";
            int track = 0;
            int delayHours = 1;
            int delayMinutes = 15;

            // Act
            new TrainDeparture(departureTimeHours, departureTimeMinutes, line,
                    trainNumber, destination, track, delayHours, delayMinutes);

            // Assert
            fail("The test 'throwsOnInvalidTrainNumber' failed since it did not throw"
                + " 'Ill.Arg.Exc.' as expected.");
        } catch (IllegalArgumentException e) {
            assertEquals("Invalid track number. Has to be positive or -1.", e.getMessage());
        }
    }

    @Test
    @DisplayName("TrainDeparture-constructor throws 'Ill.Arg.Exc.' on invalid delayed departure time")
    public void throwsOnInvalidDelayedDepartureTime() {
        try {
            // Arrange
            int departureTimeHours = 12;
            int departureTimeMinutes = 30;
            String line = "L1";
            int trainNumber = 1;
            String destination = "TestDestination";
            int track = 1;
            int delayHours = -1;
            int delayMinutes = -15;

            // Act
            new TrainDeparture(departureTimeHours, departureTimeMinutes, line,
                    trainNumber, destination, track, delayHours, delayMinutes);

            // Assert
            fail("The test 'throwsOnInvalidTrainNumber' failed since it did not throw"
                + " 'Ill.Arg.Exc.' as expected.");
        } catch (IllegalArgumentException e) {
            assertEquals("Delay hours and minutes must be non-negative.", e.getMessage());
        }
    }
  }

  @Nested
  @DisplayName("Positive tests for 'TrainDeparture'")
  public class methodsDoesNotThrowException {

      @Test
      @DisplayName("TrainDeparture-constructor creates a train departure with throwing 'Ill.Arg.Exc.'")
      public void doesNotThrowException() {
          try {
              // Arrange
              int departureTimeHours = 12;
              int departureTimeMinutes = 30;
              String line = "L1";
              int trainNumber = 1;
              String destination = "TestDestination";
              int track = 1;
              int delayHours = 1;
              int delayMinutes = 15;

              // Act
              TrainDeparture testTrain = new TrainDeparture(departureTimeHours, departureTimeMinutes, line,
                                         trainNumber, destination, track, delayHours, delayMinutes);

              // Assert
              assertNotNull(testTrain);
          } catch (IllegalArgumentException e) {
              fail("The test failed with the exception-message " + e.getMessage());
          }
      }

      @Test
      @DisplayName("ToString method prints the train departure as expected")
      public void toStringPrintsCorrectly() {
          // Arrange
          int departureTimeHours = 12;
          int departureTimeMinutes = 30;
          String line = "L1";
          int trainNumber = 1;
          String destination = "TestDestination";
          int track = 1;
          int delayHours = 1;
          int delayMinutes = 15;

          // Act
          TrainDeparture testTrain = new TrainDeparture(departureTimeHours, departureTimeMinutes, line,
                                     trainNumber, destination, track, delayHours, delayMinutes);

          String check = LocalTime.of(12,30) + "   " + line + "   " + destination
                  + "   Train number: " + trainNumber + "   Track: " + track + "   Delayed, new time: "
                  + LocalTime.of(12, 30).plusHours(delayHours).plusMinutes(delayMinutes);

          // Assert
          assertEquals(check, testTrain.toString());
          if (!(check.equals(testTrain.toString()))) {
              fail("The toString method for the item did not print the expected string");
          }
      }
  }
}