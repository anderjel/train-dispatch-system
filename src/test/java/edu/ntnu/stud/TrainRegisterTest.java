package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class TrainRegisterTest {

    @Nested
    @DisplayName("Negative tests for 'TrainRegister', throws exceptions on wrong input parameters")
    public class methodsThrowsExceptions {

        @Test
        @DisplayName("Test for addTrainDeparture, returns message if train number already exists")
        public void testAddDuplicateTrainDeparture() {
            // Arrange
            TrainRegister register = new TrainRegister();
            int departureTimeHours = 1;
            int departureTimeMinutes = 30;
            String line = "L1";
            int trainNumber = 1;
            String destination = "TestDestination";
            int track = 3;
            int delayHours = 1;
            int delayMinutes = 15;

            // Act
            register.addTrainDeparture(departureTimeHours, departureTimeMinutes, line,
                    trainNumber, destination, track, delayHours, delayMinutes);

            // Assert
            assertEquals("Train number " + trainNumber + " already exists.",
                    register.addTrainDeparture(departureTimeHours, departureTimeMinutes, line,
                            trainNumber, destination, track, delayHours, delayMinutes));
        }

        @Test
        @DisplayName("TrainRegister addTrainDeparture() throws 'Ill.Arg.Exc.' on invalid Departure time")
        public void addTrainDepartureThrowsOnInvalidDepartureTime() {
            // Arrange
            TrainRegister register = new TrainRegister();
            int departureTimeHours = -1;
            int departureTimeMinutes = 30;
            String line = "L1";
            int trainNumber = 1;
            String destination = "TestDestination";
            int track = 3;
            int delayHours = 1;
            int delayMinutes = 15;

            // Assert
            assertEquals("Error: Invalid departure time.",
                    register.addTrainDeparture(departureTimeHours, departureTimeMinutes, line,
                            trainNumber, destination, track, delayHours, delayMinutes));

        }

        @Test
        @DisplayName("Test for removeTrainDeparture, returns message if inputted train number does not exists")
        public void removeTrainDepartureNotExistingTrainNumber() {
            // Arrange
            TrainRegister register = new TrainRegister();
            int departureTimeHours = 1;
            int departureTimeMinutes = 30;
            String line = "L1";
            int trainNumber = 1;
            String destination = "TestDestination";
            int track = 3;
            int delayHours = 1;
            int delayMinutes = 15;

            // Act
            register.addTrainDeparture(departureTimeHours, departureTimeMinutes, line,
                    trainNumber, destination, track, delayHours, delayMinutes);

            // Assert
            assertEquals("There is no train departure with train number " + (trainNumber + 1) + ".",
                    register.removeTrainDeparture(trainNumber + 1));
        }

        @Test
        @DisplayName("Test for findTrainDeparture, returns message if inputted train number does not exists")
        public void findTrainDepartureNotExistingTrainNumber() {
            // Arrange
            TrainRegister register = new TrainRegister();
            int departureTimeHours = 1;
            int departureTimeMinutes = 30;
            String line = "L1";
            int trainNumber = 1;
            String destination = "TestDestination";
            int track = 3;
            int delayHours = 1;
            int delayMinutes = 15;

            // Act
            register.addTrainDeparture(departureTimeHours, departureTimeMinutes, line,
                    trainNumber, destination, track, delayHours, delayMinutes);

            // Assert
            assertEquals("There is no train departure with train number " + (trainNumber + 1) + ".",
                    register.findTrainDeparture(trainNumber + 1));
        }

        @Test
        @DisplayName("Test for updateTime, returns message if updated time is earlier than current time")
        public void updateTimeCurrentTimeBeforeUpdateTime() {
            // Arrange
            TrainRegister register = new TrainRegister();
            int departureTimeHours = 5;
            int departureTimeMinutes = 30;

            // Act
            register.updateTime(12, 0);

            // Assert
            assertEquals("It is not possible to set the time earlier than the current time (" + LocalTime.of(12, 0) + ").",
                    register.updateTime(departureTimeHours, departureTimeMinutes));
        }
    }

    @Nested
    @DisplayName("Positive tests for 'TrainRegister'-class")
    public class methodsDoesNotThrowException {

        @Test
        @DisplayName("TrainRegister addTrainDeparture adds a train departure to the register")
        public void trainRegisterAddTrainDeparture() {
            // Arrange
            TrainRegister register = new TrainRegister();
            int departureTimeHours = 1;
            int departureTimeMinutes = 30;
            String line = "L1";
            int trainNumber = 1;
            String destination = "TestDestination";
            int track = 3;
            int delayHours = 1;
            int delayMinutes = 15;

            // Assert
            assertEquals("Train departure has been added.",
                    register.addTrainDeparture(departureTimeHours, departureTimeMinutes, line,
                            trainNumber, destination, track, delayHours, delayMinutes));

        }

        @Test
        @DisplayName("TrainRegister removeTrainDeparture removes a train departure from the register")
        public void trainRegisterRemoveTrainDeparture() {
            // Arrange
            TrainRegister register = new TrainRegister();
            int departureTimeHours = 1;
            int departureTimeMinutes = 30;
            String line = "L1";
            int trainNumber = 1;
            String destination = "TestDestination";
            int track = 3;
            int delayHours = 1;
            int delayMinutes = 15;

            // Act
            register.addTrainDeparture(departureTimeHours, departureTimeMinutes, line,
                    trainNumber, destination, track, delayHours, delayMinutes);

            // Assert
            assertEquals("Train departure with train number " + trainNumber + " has been removed.",
                    register.removeTrainDeparture(trainNumber));

        }

        @Test
        @DisplayName("TrainRegister assignTrack() assigns a track to a train departure")
        public void trainRegisterAssignTrack() {
            // Arrange
            TrainRegister register = new TrainRegister();
            int departureTimeHours = 1;
            int departureTimeMinutes = 30;
            String line = "L1";
            int trainNumber = 1;
            String destination = "TestDestination";
            int track = 3;
            int delayHours = 1;
            int delayMinutes = 15;

            // Act
            register.addTrainDeparture(departureTimeHours, departureTimeMinutes, line,
                    trainNumber, destination, track, delayHours, delayMinutes);

            // Assert
            assertEquals("Train departure with train number " + trainNumber
                    + " has been assigned to track " + track, register.assignTrack(trainNumber, track));
        }

        @Test
        @DisplayName("TrainRegister enterDelay() adds delay to a train departure's departure time")
        public void trainRegisterEnterDelay() {
            // Arrange
            TrainRegister register = new TrainRegister();
            int departureTimeHours = 1;
            int departureTimeMinutes = 30;
            String line = "L1";
            int trainNumber = 1;
            String destination = "TestDestination";
            int track = 3;
            int delayHours = 1;
            int delayMinutes = 15;

            // Act
            register.addTrainDeparture(departureTimeHours, departureTimeMinutes, line,
                    trainNumber, destination, track, delayHours, delayMinutes);

            // Assert
            assertEquals("Train departure with train number " + trainNumber
                    + " has been delayed by " + delayHours + " hours and " + delayMinutes + " minutes.",
                    register.enterDelay(trainNumber, delayHours, delayMinutes));
        }

        @Test
        @DisplayName("TrainRegister findTrainDeparture() finds and returns a train departure based on train number")
        public void trainRegisterFindTrainDeparture() {
            // Arrange
            TrainRegister register = new TrainRegister();
            int departureTimeHours = 1;
            int departureTimeMinutes = 30;
            String line = "L1";
            int trainNumber = 1;
            String destination = "TestDestination";
            int track = 3;
            int delayHours = 1;
            int delayMinutes = 15;

            // Act
            register.addTrainDeparture(departureTimeHours, departureTimeMinutes, line,
                    trainNumber, destination, track, delayHours, delayMinutes);

            // Assert
            String check = LocalTime.of(1,30) + "   " + line + "   " + destination
                    + "   Train number: " + trainNumber + "   Track: " + track + "   Delayed, new time: "
                    + LocalTime.of(1, 30).plusHours(delayHours).plusMinutes(delayMinutes);
            assertEquals(check, register.findTrainDeparture(trainNumber));
        }

        @Test
        @DisplayName("TrainRegister findTrainDepartureBasedOnDestination() finds and returns all train departures based on destination")
        public void trainRegisterFindTrainDepartureBasedOnDestination() {
            // Arrange
            TrainRegister register = new TrainRegister();
            int departureTimeHours = 1;
            int departureTimeMinutes = 30;
            String line = "L1";
            int trainNumber = 1;
            String destination = "TestDestination";
            int track = 3;
            int delayHours = 1;
            int delayMinutes = 15;

            // Act
            register.addTrainDeparture(departureTimeHours, departureTimeMinutes, line,
                    trainNumber, destination, track, delayHours, delayMinutes);

            // Assert
            String check = LocalTime.of(1,30) + "   " + line + "   " + destination
                    + "   Train number: " + trainNumber + "   Track: " + track + "   Delayed, new time: "
                    + LocalTime.of(1, 30).plusHours(delayHours).plusMinutes(delayMinutes) + "\n";
            assertEquals(check, register.findTrainDeparturesBasedOnDestination(destination));
        }

        @Test
        @DisplayName("TrainRegister updateTime() updates the LocalTime for the train register.")
        public void trainRegisterUpdateTime() {
            // Arrange
            TrainRegister register = new TrainRegister();
            int departureTimeHours = 5;
            int departureTimeMinutes = 30;

            // Assert
            assertEquals("The time has been updated to " + LocalTime.of(departureTimeHours, departureTimeMinutes) + ".",
                    register.updateTime(departureTimeHours, departureTimeMinutes));
        }

        @Test
        @DisplayName("TrainRegister sortedTrainDeparturesByTime sorts the current train register by departure time (excluded delay).")
        public void trainRegisterSortedTrainDeparturesByTime() {
            // Arrange
            TrainRegister register = new TrainRegister();
            int departureTimeHours = 1;
            int departureTimeMinutes = 30;
            String line = "L1";
            int trainNumber = 1;
            String destination = "TestDestination";
            int track = 3;
            int delayHours = 1;
            int delayMinutes = 15;

            // Act
            register.addTrainDeparture(departureTimeHours, departureTimeMinutes, line,
                    trainNumber, destination, track, delayHours, delayMinutes);

            // Assert
            String check = "Departures" + "        Time: " + LocalTime.of(0, 0) + "\n\n"
                    + LocalTime.of(1,30) + "   " + line + "   " + destination
                    + "   Train number: " + trainNumber + "   Track: " + track + "   Delayed, new time: "
                    + LocalTime.of(1, 30).plusHours(delayHours).plusMinutes(delayMinutes) + "\n";
            assertEquals(check, register.sortedTrainDeparturesByTime());
        }
    }
}
